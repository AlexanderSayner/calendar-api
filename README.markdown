# Calendar demo API

Application programming interface for calendar graphical standalone desktop application.

## Releases

### Builds

v0.0.1-SNAPSHOT - Initial version of the application  
v1.0.0-Stable - Project template (1.0)  
v0.1.0-Develop - Refactoring of the first stable release  
v0.1.1-Develop - Functionality developing  
v1.1.0-Stable - Basic functionality release (1.1)  
v1.2.0-Develop - Future changes (Early Access)  
v1.2.0-Stable - Future release

### Version naming

There is absolute no way to delete any old method in newer version. You have to create a new one instead.
DO NOT BRAKE ANY WORKING LOGIC FROM OLDER STABLE VERSION! The idea is to never force any user to update from first
stable to any newer stable.  
Template where type 1 is 'M' type 2 is 'm' type 3 is e type 4 is '###' there # is a number:  
vM.m.e.###  
Example: v5.1.12.001

#### **Type 1: major software releases**

Dramatic changes in application interface or logic as well as deprecation of a half application interfaces.

#### **Type 2: minor software releases**

Includes new features in application interface and logic reworking. Should include long term support versions.

#### **Type 3: emergency software releases**

Hot fixes which ones have no impact of any logic or interface.

#### **Type 4: work in progress changes**

There is might be type 4 part of version naming just to make sure that any tiny changes produces unique version number.

##### *Source*

```http request
https://www.parkersoftware.com/blog/the-three-software-release-types-and-what-they-mean-for-users/
```

## Installation guide

### **Database setup**

Clone docker configuration for PostgreSQL 9.6

```shell
git clone https://github.com/postgresqlaas/docker-postgresql.git && cd docker-postgresql/
```

Build docker image

```shell
docker build -t frodenas/postgresql .
```

Create and run docker container

```shell
docker run -d     --name postgresql     -p 6432:5432     -e POSTGRES_USERNAME=sandbox     -e POSTGRES_PASSWORD=secret     -e POSTGRES_DBNAME=cms     -e POSTGRES_EXTENSIONS=citext     frodenas/postgresql
```

Now PostgreSQL container is running.  
To stop docker container:

```shell
docker stop $(docker ps -aqf name=postgresql)
```

To start docker container:

```shell
docker start $(docker ps -aqf name=postgresql)
```

### **Running project**

Clone from git repository and run throw your Java IDE

#### JVM Options

-Duser.timezone="UTC"

## Debugging

### Health check sql scripts

Postgres server version

```postgresql
SELECT version();
```

Timezone name list

```postgresql
SELECT name,
       abbrev,
       utc_offset,
       is_dst
FROM pg_timezone_names;
```
