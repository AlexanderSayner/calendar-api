package org.sandbox.calendar.api.configuration.datasource;

import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

/**
 * Postgres datasource configuration
 *
 * @author  archon
 * @version 1.0
 * @since   1.0
 */
@Configuration
public class DataSourceConfig {
    @Bean
    public DataSource dataSource() {
        final DataSourceBuilder<?> dataSourceBuilder = DataSourceBuilder.create();
        dataSourceBuilder.driverClassName("org.postgresql.Driver");
        dataSourceBuilder.url("jdbc:postgresql://localhost:6432/calendar");
        dataSourceBuilder.username("sandbox");
        dataSourceBuilder.password("secret");
        return dataSourceBuilder.build();
    }
}
