package org.sandbox.calendar.api.configuration.datasource;

import org.flywaydb.core.Flyway;
import org.flywaydb.core.api.configuration.FluentConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Flyway migration system configuration
 *
 * @author  archon
 * @version 1.0
 * @since   1.0
 */
@Configuration
public class MigrationConfiguration {
    @Bean
    public Flyway flyway() {
        FluentConfiguration configure = Flyway
                .configure()
                .dataSource("jdbc:postgresql://localhost:6432/calendar", "sandbox", "secret")
                .validateMigrationNaming(true);
        return new Flyway(configure);
    }
}
