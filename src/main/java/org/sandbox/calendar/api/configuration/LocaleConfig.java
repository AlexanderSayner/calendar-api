package org.sandbox.calendar.api.configuration;

import jakarta.annotation.PostConstruct;

import static java.util.TimeZone.getTimeZone;
import static java.util.TimeZone.setDefault;

/**
 * App timezone configuration
 *
 * @author  archon
 * @version 1.0
 * @since   1.0
 */
public class LocaleConfig {
    @PostConstruct
    public void init() {
        setDefault(getTimeZone("UTC"));
    }
}
