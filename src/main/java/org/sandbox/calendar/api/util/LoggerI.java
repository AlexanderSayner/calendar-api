package org.sandbox.calendar.api.util;

import org.slf4j.LoggerFactory;

/**
 * Default logger getter
 *
 * @author  archon
 * @version 1.0
 * @since   1.0
 */
public interface LoggerI {
    default org.slf4j.Logger getLogger() {
        return LoggerFactory.getLogger(getClass());
    }
}
