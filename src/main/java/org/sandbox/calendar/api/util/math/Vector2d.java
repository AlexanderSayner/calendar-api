package org.sandbox.calendar.api.util.math;

import java.io.Serializable;
import java.util.Objects;

/**
 * Numeric two dimension vector
 *
 * @param <T>   numeric type
 * @author      archon
 * @version     1.0
 * @since       1.0
 */
public class Vector2d<T extends Number> implements Serializable {
    public static final Vector2d<Integer> empty = new Vector2d<>(0, 0);
    private T x;
    private T y;

    public Vector2d(T a) {
        this.x = a;
        this.y = a;
    }

    public Vector2d(T x, T y) {
        this.x = x;
        this.y = y;
    }

    public T getX() {
        return x;
    }

    public void setX(T x) {
        this.x = x;
    }

    public T getY() {
        return y;
    }

    public void setY(T y) {
        this.y = y;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Vector2d<?> vector2d = (Vector2d<?>) o;
        return Objects.equals(x, vector2d.x) && Objects.equals(y, vector2d.y);
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }

    @Override
    public String toString() {
        return "Vector2d{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }
}
