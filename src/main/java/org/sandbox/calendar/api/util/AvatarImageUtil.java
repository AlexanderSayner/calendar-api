package org.sandbox.calendar.api.util;

import java.util.Optional;

/**
 * File related operations
 *
 * @author  archon
 * @version 1.0
 * @since   1.0
 */
public class AvatarImageUtil {
    /**
     * Gets any file extension
     *
     * @param fileName must include dot
     * @return last file name part after dot
     */
    public static Optional<String> getFileExtension(String fileName) {
        final int indexOfLastDot = fileName.lastIndexOf('.');

        if (indexOfLastDot == -1) {
            return Optional.empty();
        } else {
            return Optional.of(fileName.substring(indexOfLastDot + 1));
        }
    }
}
