package org.sandbox.calendar.api.domain.web.dto;

import java.util.List;
import java.util.Objects;

/**
 * @author  archon
 * @version 1.0
 * @since   1.0
 */
public class FolderTableDto {
    private Integer id;
    private Integer columns;
    private Integer rows;
    private List<FolderTableCellDto> cells;

    public FolderTableDto() {
    }

    public FolderTableDto(Integer id, Integer columns, Integer rows, List<FolderTableCellDto> cells) {
        this.id = id;
        this.columns = columns;
        this.rows = rows;
        this.cells = cells;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getColumns() {
        return columns;
    }

    public void setColumns(Integer columns) {
        this.columns = columns;
    }

    public Integer getRows() {
        return rows;
    }

    public void setRows(Integer rows) {
        this.rows = rows;
    }

    public List<FolderTableCellDto> getCells() {
        return cells;
    }

    public void setCells(List<FolderTableCellDto> cells) {
        this.cells = cells;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FolderTableDto that = (FolderTableDto) o;
        return Objects.equals(id, that.id) && Objects.equals(columns, that.columns) && Objects.equals(rows, that.rows) && Objects.equals(cells, that.cells);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, columns, rows, cells);
    }

    @Override
    public String toString() {
        return "FolderTableDto{" +
                "id=" + id +
                ", columns=" + columns +
                ", rows=" + rows +
                ", cells=" + cells +
                '}';
    }
}
