package org.sandbox.calendar.api.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Objects;

/**
 * User of the desktop calendar application
 *
 * @author  archon
 * @version 1.0
 * @since   1.0
 */
@Entity
@Table(name = "users")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;
    private String avatar;
    private ZonedDateTime date;
    @OneToMany(mappedBy = "user")
    @JsonIgnore
    private List<Task> tasks;
    @OneToMany(mappedBy = "user")
    @JsonIgnore
    private List<Folder> folders;

    public User() {
    }

    public User(Integer id, String name, String avatar, ZonedDateTime date) {
        this.id = id;
        this.name = name;
        this.avatar = avatar;
        this.date = date;
    }

    public User(Integer id,
                String name,
                String avatar,
                ZonedDateTime date,
                List<Task> tasks,
                List<Folder> folders) {
        this.id = id;
        this.name = name;
        this.avatar = avatar;
        this.date = date;
        this.tasks = tasks;
        this.folders = folders;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public ZonedDateTime getDate() {
        return date;
    }

    public void setDate(ZonedDateTime date) {
        this.date = date;
    }

    public List<Task> getTasks() {
        return tasks;
    }

    public void setTasks(List<Task> tasks) {
        this.tasks = tasks;
    }

    public List<Folder> getFolders() {
        return folders;
    }

    public void setFolders(List<Folder> folders) {
        this.folders = folders;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(id, user.id) && Objects.equals(name, user.name) && Objects.equals(avatar, user.avatar) && Objects.equals(date, user.date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, avatar, date);
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", avatar='" + avatar + '\'' +
                ", date=" + date +
                '}';
    }
}
