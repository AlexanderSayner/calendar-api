package org.sandbox.calendar.api.domain.entity;


import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import org.sandbox.calendar.api.domain.service.CsvField;

import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * Basic task entity
 *
 * @author  archon
 * @version 1.0
 * @since   1.0
 */
@Entity
@Table(name = "tasks")
public class Task {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;
    private String description;
    private ZonedDateTime date;
    @ManyToOne
    @JoinColumn(name = "user_fk")
    @JsonIgnore
    private User user;

    public Task() {
    }

    public Task(Integer id, String name, String description, ZonedDateTime date) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.date = date;
    }

    public Task(Integer id, String name, String description, ZonedDateTime date, User user) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.date = date;
        this.user = user;
    }

    @CsvField(sort = 1)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @CsvField
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @CsvField
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @CsvField
    public ZonedDateTime getDate() {
        return date;
    }

    public void setDate(ZonedDateTime date) {
        this.date = date;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Task task = (Task) o;
        return Objects.equals(id, task.id) && Objects.equals(name, task.name) && Objects.equals(description, task.description) && Objects.equals(date, task.date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, description, date);
    }

    @Override
    public String toString() {
        return "Task{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", date=" + date +
                '}';
    }
}
