package org.sandbox.calendar.api.domain.web.dto;

import java.util.Objects;

/**
 * @author  archon
 * @version 1.0
 * @since   1.0
 */
public class FolderTableCellDto {
    private Integer id;
    private Integer column;
    private Integer row;
    private String text;

    public FolderTableCellDto() {
    }

    public FolderTableCellDto(Integer id, Integer column, Integer row, String text) {
        this.id = id;
        this.column = column;
        this.row = row;
        this.text = text;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getColumn() {
        return column;
    }

    public void setColumn(Integer column) {
        this.column = column;
    }

    public Integer getRow() {
        return row;
    }

    public void setRow(Integer row) {
        this.row = row;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FolderTableCellDto that = (FolderTableCellDto) o;
        return Objects.equals(column, that.column) && Objects.equals(row, that.row) && Objects.equals(text, that.text);
    }

    @Override
    public int hashCode() {
        return Objects.hash(column, row, text);
    }

    @Override
    public String toString() {
        return "FolderTableCellDto{" +
                "id=" + id +
                ", column=" + column +
                ", row=" + row +
                ", text='" + text + '\'' +
                '}';
    }
}
