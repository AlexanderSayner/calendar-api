package org.sandbox.calendar.api.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;

import java.util.List;
import java.util.Objects;

/**
 * Table in a users folder
 *
 * @author  archon
 * @version 1.0
 * @since   1.0
 */
@Entity
@Table(name = "folder_tables")
public class FolderTable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name = "table_columns")
    private Integer columns;
    @Column(name = "table_rows")
    private Integer rows;
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "folder_fk")
    @JsonIgnore
    private Folder folder;
    @OneToMany(mappedBy = "table")
    @JsonIgnore
    private List<FolderTableCell> cells;

    public FolderTable() {
    }

    public FolderTable(Integer id, Integer columns, Integer rows) {
        this.id = id;
        this.columns = columns;
        this.rows = rows;
    }

    public FolderTable(Integer id,
                       Integer columns,
                       Integer rows,
                       Folder folder,
                       List<FolderTableCell> cells) {
        this.id = id;
        this.columns = columns;
        this.rows = rows;
        this.folder = folder;
        this.cells = cells;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getColumns() {
        return columns;
    }

    public void setColumns(Integer columns) {
        this.columns = columns;
    }

    public Integer getRows() {
        return rows;
    }

    public void setRows(Integer rows) {
        this.rows = rows;
    }

    public Folder getFolder() {
        return folder;
    }

    public void setFolder(Folder folder) {
        this.folder = folder;
    }

    public List<FolderTableCell> getCells() {
        return cells;
    }

    public void setCells(List<FolderTableCell> cells) {
        this.cells = cells;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FolderTable that = (FolderTable) o;
        return Objects.equals(id, that.id) && Objects.equals(columns, that.columns) && Objects.equals(rows, that.rows);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, columns, rows);
    }

    @Override
    public String toString() {
        return "FolderTable{" +
                "id=" + id +
                ", columns=" + columns +
                ", rows=" + rows +
                '}';
    }
}
