package org.sandbox.calendar.api.domain.web;

import jakarta.servlet.http.HttpServletResponse;
import org.apache.commons.io.IOUtils;
import org.sandbox.calendar.api.util.LoggerI;
import org.slf4j.Logger;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Files manager
 *
 * @author  archon
 * @version 1.0
 * @since   1.0
 */
@RestController
@RequestMapping("files")
public class FilesController implements LoggerI {
    private final Logger logger = getLogger();

    /**
     * Download file from output folder
     *
     * @param fileName with extension
     * @param response cause download dialog in browser
     */
    @GetMapping("{file_name}")
    public void getFile(
            @PathVariable("file_name") String fileName,
            HttpServletResponse response) {
        try {
            // get your file as InputStream
            final File initialFile = new File("output" + '/' + fileName);
            final InputStream is = new FileInputStream(initialFile);
            // copy it to response's OutputStream
            final int copy = IOUtils.copy(is, response.getOutputStream());
            logger.info("File size is {} bytes", copy);
            response.flushBuffer();
        } catch (IOException ex) {
            logger.info("Error writing file to output stream. Filename was '{}': {}", fileName, ex.getMessage());
            throw new RuntimeException("IOError writing file to output stream");
        }
    }
}
