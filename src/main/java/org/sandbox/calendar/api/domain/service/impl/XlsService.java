package org.sandbox.calendar.api.domain.service.impl;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.sandbox.calendar.api.domain.service.XlsServiceI;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

/**
 * User cases are identical with parent csv generator class
 * <a href="https://www.baeldung.com/java-microsoft-excel">...</a>
 *
 * @author  archon
 * @version 1.0
 * @see     org.sandbox.calendar.api.domain.service.impl.CsvService
 * @since   1.0
 */
@Service("xls")
public class XlsService extends CsvService implements XlsServiceI {
    private final Logger logger = getLogger();
    private final String sheetName;
    private final String width;

    public XlsService(@Value("${xls.sheet.name}") String sheetName,
                      @Value("${xls.column.width}") String width) {
        this.sheetName = sheetName;
        this.width = width;
    }

    @Override
    public <T> boolean go(List<T> data, Class<T> elementClassType) {
        final List<String[]> dataLines = go(data,
                getAnnotatedGetters(elementClassType));
        return givenDataArrayWriteIntoXls(dataLines);
    }

    /**
     * Form table structure
     *
     * @param dataLines content
     * @return xls data-structure ready to write
     */
    protected Workbook getReady(List<String[]> dataLines) {
        final XSSFWorkbook workbook = new XSSFWorkbook();

        final Sheet sheet = workbook.createSheet(sheetName);
        for (int i = 0; i < dataLines.get(0).length; i++) {
            sheet.setColumnWidth(i, Integer.parseInt(width));
        }

        // Header

        final Row header = sheet.createRow(0);

        final CellStyle headerStyle = workbook.createCellStyle();
        headerStyle.setFillForegroundColor(IndexedColors.CORAL.getIndex());
        headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

        final XSSFFont font = workbook.createFont();
        font.setFontName("Arial");
        font.setFontHeightInPoints((short) 16);
        font.setBold(true);
        headerStyle.setFont(font);

        int y = 0;
        for (final String field : dataLines.get(0)) {
            final Cell headerCell = header.createCell(y++);
            headerCell.setCellValue(field);
            headerCell.setCellStyle(headerStyle);
        }

        // Content

        final CellStyle style = workbook.createCellStyle();
        style.setWrapText(true);

        for (int i = 1; i < dataLines.size(); i++) {
            final Row row = sheet.createRow(i);
            int j = 0;
            for (final String datum : dataLines.get(i)) {
                final Cell cell = row.createCell(j++);
                cell.setCellValue(datum);
                cell.setCellStyle(style);
            }

        }

        return workbook;
    }

    /**
     * Write data to xls file on local storage
     *
     * @param dataLines table shape structured data
     * @return true if successfully wrote onto disk, otherwise false
     */
    protected boolean givenDataArrayWriteIntoXls(List<String[]> dataLines) {
        final Workbook workbook = getReady(dataLines);

        // Write to file

        final File directory = new File(OUTPUT_FILE_DIR);
        final String path = directory.getAbsolutePath();
        final String fileLocation = path + '/' + "temp.xls";

        FileOutputStream outputStream;
        try {
            outputStream = new FileOutputStream(fileLocation);
            workbook.write(outputStream);
            workbook.close();
        } catch (IOException e) {
            logger.warn("Error writing xls file: {}", e.getMessage());
            return false;
        }

        return true;
    }
}
