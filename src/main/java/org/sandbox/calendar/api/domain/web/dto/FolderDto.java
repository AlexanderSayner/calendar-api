package org.sandbox.calendar.api.domain.web.dto;

import java.util.List;
import java.util.Objects;

/**
 * @author  archon
 * @version 1.0
 * @since   1.0
 */
public class FolderDto {
    private Integer id;
    private String title;
    private Integer userId;
    private List<FolderImageDto> image;
    private FolderTableDto table;

    public FolderDto() {
    }

    public FolderDto(Integer id, String title, Integer userId, List<FolderImageDto> image, FolderTableDto table) {
        this.id = id;
        this.title = title;
        this.userId = userId;
        this.image = image;
        this.table = table;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public List<FolderImageDto> getImage() {
        return image;
    }

    public void setImage(List<FolderImageDto> image) {
        this.image = image;
    }

    public FolderTableDto getTable() {
        return table;
    }

    public void setTable(FolderTableDto table) {
        this.table = table;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FolderDto folderDto = (FolderDto) o;
        return Objects.equals(id, folderDto.id) && Objects.equals(title, folderDto.title) && Objects.equals(userId, folderDto.userId) && Objects.equals(image, folderDto.image) && Objects.equals(table, folderDto.table);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title, userId, image, table);
    }

    @Override
    public String toString() {
        return "FolderDto{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", userId=" + userId +
                ", image=" + image +
                ", table=" + table +
                '}';
    }
}
