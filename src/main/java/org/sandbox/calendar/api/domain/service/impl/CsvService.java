package org.sandbox.calendar.api.domain.service.impl;

import jakarta.annotation.PostConstruct;
import org.sandbox.calendar.api.domain.service.CsvField;
import org.sandbox.calendar.api.domain.service.CsvServiceI;
import org.sandbox.calendar.api.domain.web.UserController;
import org.sandbox.calendar.api.util.LoggerI;
import org.slf4j.Logger;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Csv file export logic
 * Use annotation to mark getters as writable to cs file
 *
 * @author  archon
 * @version 1.0
 * @see     org.sandbox.calendar.api.domain.service.CsvField
 * @since   1.0
 */
@Service("csv")
@Primary
public class CsvService implements CsvServiceI, LoggerI {
    protected static final String OUTPUT_FILE_DIR = "output";
    private static final String CSV_FILE_NAME = "task.csv";
    private final Logger logger = getLogger();

    /**
     * Note: the same logic exists in
     *
     * @see UserController#ensureDirectoryExists()
     */
    @PostConstruct
    void init() {
        final String path = OUTPUT_FILE_DIR;
        final Path dir = Path.of(OUTPUT_FILE_DIR);
        if (!Files.exists(dir)) {
            try {
                Files.createDirectories(dir);
                logger.warn("Created output directory: {}", path);
            } catch (IOException e) {
                logger.error("Error creating output directory: {}", e.getMessage());
            }
        } else {
            logger.info("Output directory exists: {}", path);
        }
    }

    /**
     * Extracts annotation marked getters
     *
     * @param entity type of class to work with
     * @param <E>    generalised type of object to write into csv file
     * @return marked getters
     */
    protected  <E> List<Method> getAnnotatedGetters(Class<E> entity) {
        final List<Method> methods = Arrays.asList(entity.getDeclaredMethods());
        return methods.stream()
                .filter(method -> Arrays.stream(method.getDeclaredAnnotations())
                        .anyMatch(annotation -> annotation.annotationType().getCanonicalName()
                                .equals(CsvField.class.getCanonicalName())
                                && method.getName().contains("get")))
                .sorted(Comparator.comparingInt(m -> m.getAnnotation(CsvField.class).sort() * -1))
                .toList();
    }

    /**
     * Form CSV content structure
     *
     * @param data    list of data objects to write
     * @param getters methods for getting fields data
     * @param <E>     any object with annotated getters
     * @return data structure ready to write
     */
    protected  <E> List<String[]> go(List<E> data, List<Method> getters) {
        final List<String[]> dataLines = new ArrayList<>();
        final List<String> header = new ArrayList<>();
        // Extract pure alias from getter name
        for (final Method method : getters) {
            final String fieldName = method.getName().toLowerCase().substring(3);
            header.add(fieldName);
        }
        // Add header as first line
        dataLines.add(header.toArray(new String[0]));
        // Add data
        for (final E datum : data) {
            final List<String> row = new ArrayList<>();
            for (final Method getter : getters) {
                try {
                    final Object invoke = getter.invoke(datum);
                    if (invoke != null) {
                        row.add(invoke.toString());
                    } else {
                        row.add("");
                    }
                } catch (IllegalAccessException | InvocationTargetException e) {
                    logger.error("Invoking method {} exception: {}", getter.getName(), e.getMessage());
                    throw new RuntimeException(e);
                }
            }
            dataLines.add(row.toArray(new String[0]));
        }
        return dataLines;
    }

    /**
     * Join array with comma separator
     *
     * @param data raw data
     * @return joined array to single string
     */
    protected String convertToCSV(String[] data) {
        return Stream.of(data)
                .map(this::escapeSpecialCharacters)
                .collect(Collectors.joining(","));
    }

    /**
     * Fields containing commas or quotes will be surrounded by double quotes,
     * and double quotes will be escaped with double quotes.
     *
     * @param data raw data
     * @return validated data
     */
    protected String escapeSpecialCharacters(String data) {
        String escapedData = data.replaceAll("\\R", " ");
        if (data.contains(",") || data.contains("\"") || data.contains("'")) {
            data = data.replace("\"", "\"\"");
            escapedData = "\"" + data + "\"";
        }
        return escapedData;
    }

    /**
     * Generated csv file
     *
     * @param dataLines content to write
     * @return true if generated file exists, false otherwise
     * @throws IOException writing to the file system failure
     */
    protected boolean givenDataArray_whenConvertToCSV_thenOutputCreated(List<String[]> dataLines) throws IOException {
        final File csvOutputFile = new File(OUTPUT_FILE_DIR + '/' + CSV_FILE_NAME);
        try (PrintWriter pw = new PrintWriter(csvOutputFile)) {
            dataLines.stream()
                    .map(this::convertToCSV)
                    .forEach(pw::println);
        }
        return csvOutputFile.exists();
    }

    /**
     * Writes given data to csv file
     *
     * @param data             list of objects to write
     * @param elementClassType class type
     * @param <T>              any object
     * @return true if success, false otherwise
     */
    @Override
    public <T> boolean go(List<T> data, Class<T> elementClassType) {
        boolean result;
        final List<String[]> dataLines = go(data,
                getAnnotatedGetters(elementClassType));
        try {
            result = givenDataArray_whenConvertToCSV_thenOutputCreated(dataLines);
        } catch (IOException e) {
            logger.warn("Cannot generate csv: {}", e.getMessage());
            return false;
        }
        return result;
    }
}
