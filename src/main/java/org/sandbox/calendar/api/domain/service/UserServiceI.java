package org.sandbox.calendar.api.domain.service;

import org.sandbox.calendar.api.domain.entity.helper.UserRelatedEntityI;

/**
 * Common user related logic
 *
 * @author  archon
 * @version 1.0
 * @since   1.0
 */
public interface UserServiceI {
    /**
     * Initialise user dependency throw functional interface implemented by entity setter
     *
     * @param userId actual user id
     * @param entity implemented logic for setting up a user entity
     * @return 0 if successfully set
     */
    int setUser(Integer userId, UserRelatedEntityI entity);
}
