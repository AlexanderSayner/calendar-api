package org.sandbox.calendar.api.domain.web.dto;

import org.sandbox.calendar.api.util.math.Vector2d;

import java.util.Objects;

/**
 * @author  archon
 * @version 1.0
 * @since   1.0
 */
public class FolderImageDto {
    private Integer id;
    private String path;
    private Vector2d<Integer> resolution;
    private Integer column;
    private Integer row;

    public FolderImageDto() {
    }

    public FolderImageDto(Integer id, String path, Vector2d<Integer> resolution, Integer column, Integer row) {
        this.id = id;
        this.path = path;
        this.resolution = resolution;
        this.column = column;
        this.row = row;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Vector2d<Integer> getResolution() {
        return resolution;
    }

    public void setResolution(Vector2d<Integer> resolution) {
        this.resolution = resolution;
    }

    public Integer getColumn() {
        return column;
    }

    public void setColumn(Integer column) {
        this.column = column;
    }

    public Integer getRow() {
        return row;
    }

    public void setRow(Integer row) {
        this.row = row;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FolderImageDto that = (FolderImageDto) o;
        return Objects.equals(id, that.id) && Objects.equals(path, that.path);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, path);
    }

    @Override
    public String toString() {
        return "FolderImageDto{" +
                "id=" + id +
                ", path='" + path + '\'' +
                ", resolution=" + resolution +
                ", column=" + column +
                ", row=" + row +
                '}';
    }
}
