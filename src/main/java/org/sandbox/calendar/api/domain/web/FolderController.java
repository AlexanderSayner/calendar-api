package org.sandbox.calendar.api.domain.web;

import jakarta.websocket.server.PathParam;
import org.sandbox.calendar.api.domain.entity.Folder;
import org.sandbox.calendar.api.domain.entity.FolderTableCell;
import org.sandbox.calendar.api.domain.entity.Image;
import org.sandbox.calendar.api.domain.repository.FolderRepository;
import org.sandbox.calendar.api.domain.service.UserServiceI;
import org.sandbox.calendar.api.domain.web.dto.FolderDto;
import org.sandbox.calendar.api.domain.web.dto.FolderImageDto;
import org.sandbox.calendar.api.domain.web.dto.FolderTableCellDto;
import org.sandbox.calendar.api.domain.web.dto.FolderTableDto;
import org.sandbox.calendar.api.util.LoggerI;
import org.sandbox.calendar.api.util.math.Vector2d;
import org.slf4j.Logger;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;

/**
 * Folder application interface
 *
 * @author  archon
 * @version 1.0
 * @since   1.0
 */
@RestController
@RequestMapping("api/folder")
public class FolderController implements LoggerI {
    private final Pattern pattern = Pattern.compile("-?\\d+(\\.\\d+)?");
    private final Logger logger = getLogger();
    private final FolderRepository repository;
    private final UserServiceI userService;

    public FolderController(FolderRepository repository,
                            UserServiceI userService) {
        this.repository = repository;
        this.userService = userService;
    }

    /**
     * Full data list
     *
     * @return folders array
     */
    @GetMapping("all")
    List<Folder> folderList() {
        return repository.findAll();
    }

    /**
     * Saving folder with user
     *
     * @param userId existing id or null to save without user
     * @param folder data
     * @return saved folder id
     */
    @PostMapping("save")
    Integer save(@PathParam("userId") Integer userId, @RequestBody Folder folder) {
        if (userService.setUser(userId, folder::setUser) != 0) {
            logger.info("Saving folder without user");
        }
        return repository.save(folder).getId();
    }

    /**
     * Save image in the folder
     *
     * @param folderId id in database
     * @param imageDto image data to save
     * @return true if saved, false otherwise
     */
    @PostMapping("gallery/add/{folderId}")
    boolean addNewImageToGallery(@PathVariable String folderId, @RequestBody FolderImageDto imageDto) {
        final Optional<Folder> folder = repository.findById(Integer.valueOf(folderId));
        if (folder.isEmpty()) {
            logger.warn("No folder with id={}. Saving gallery failed", folderId);
            return false;
        }
        final Image image = folder.get().getImages().stream().filter(savedImage -> savedImage.getId().equals(imageDto.getId())).findFirst().orElseGet(Image::new);
        image.setName(imageDto.getPath());
        image.setCoordinateX(imageDto.getColumn());
        image.setCoordinateY(imageDto.getRow());
        image.setResizedResolutionX(imageDto.getResolution().getX());
        image.setResizedResolutionY(imageDto.getResolution().getY());
        return folder.get().getImages().add(image);
    }

    /**
     * Folder list for user
     *
     * @param userId nullable value
     * @return if user id is not found or equals 0 returns all records
     */
    @GetMapping
    List<FolderDto> folderList(@PathParam("userId") String userId) {
        final List<Folder> all = repository.findAll().stream()
                .filter(folder ->
                        folder != null && (
                                userId == null
                                        || userId.equals("0")
                                        || folder.getUser() == null
                                        || !pattern.matcher(userId).matches()
                                        || folder.getUser().getId().equals(Integer.valueOf(userId)))
                ).toList();
        final List<FolderDto> result = new ArrayList<>(all.size() + 1);
        for (Folder folder : all) {
            final FolderDto dto = new FolderDto();
            dto.setId(folder.getId());
            dto.setTitle(folder.getName());
            dto.setUserId(folder.getUser() != null ? folder.getUser().getId() : null);
            if (folder.getImages() != null && !folder.getImages().isEmpty()) {
                dto.setImage(new ArrayList<>());
            }
            for (final Image image : folder.getImages()) {
                final FolderImageDto folderImageDto =
                        new FolderImageDto(
                                image.getId(),
                                image.getName(),
                                new Vector2d<>(image.getResizedResolutionX(), image.getResizedResolutionY()),
                                image.getCoordinateX(),
                                image.getCoordinateY());
                dto.getImage().add(folderImageDto);
            }
            if (folder.getTable() != null) {
                final FolderTableDto folderTableDto = new FolderTableDto();
                folderTableDto.setId(folder.getTable().getId());
                folderTableDto.setColumns(folder.getTable().getColumns());
                folderTableDto.setRows(folder.getTable().getRows());
                final List<FolderTableCellDto> cells = new ArrayList<>();
                for (final FolderTableCell cell : folder.getTable().getCells()) {
                    cells.add(new FolderTableCellDto(
                            cell.getId(),
                            cell.getColumn(),
                            cell.getRow(),
                            cell.getContent()
                    ));
                }
                folderTableDto.setCells(cells);
                dto.setTable(folderTableDto);
            }
            result.add(dto);
        }
        return result;
    }
}
