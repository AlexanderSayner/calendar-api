package org.sandbox.calendar.api.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;

import java.util.Objects;

/**
 * Table cell
 *
 * @author  archon
 * @version 1.0
 * @since   1.0
 */
@Entity
@Table(name = "folder_table_cells")
public class FolderTableCell {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name="cell_column")
    private Integer column;
    @Column(name="cell_row")
    private Integer row;
    @Column(name="text_content")
    private String content;
    @ManyToOne
    @JoinColumn(name = "table_fk")
    @JsonIgnore
    private FolderTable table;

    public FolderTableCell() {
    }

    public FolderTableCell(Integer id, Integer column, Integer row, String content) {
        this.id = id;
        this.column = column;
        this.row = row;
        this.content = content;
    }

    public FolderTableCell(Integer id,
                           Integer column,
                           Integer row,
                           String content,
                           FolderTable table) {
        this.id = id;
        this.column = column;
        this.row = row;
        this.content = content;
        this.table = table;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getColumn() {
        return column;
    }

    public void setColumn(Integer column) {
        this.column = column;
    }

    public Integer getRow() {
        return row;
    }

    public void setRow(Integer row) {
        this.row = row;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public FolderTable getTable() {
        return table;
    }

    public void setTable(FolderTable table) {
        this.table = table;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FolderTableCell that = (FolderTableCell) o;
        return Objects.equals(id, that.id) && Objects.equals(column, that.column) && Objects.equals(row, that.row) && Objects.equals(content, that.content);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, column, row, content);
    }

    @Override
    public String toString() {
        return "FolderTableCell{" +
                "id=" + id +
                ", column=" + column +
                ", row=" + row +
                ", content='" + content + '\'' +
                '}';
    }
}
