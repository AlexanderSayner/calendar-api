package org.sandbox.calendar.api.domain.web;

import jakarta.annotation.PostConstruct;
import org.sandbox.calendar.api.domain.entity.User;
import org.sandbox.calendar.api.domain.repository.UserRepository;
import org.sandbox.calendar.api.util.AvatarImageUtil;
import org.sandbox.calendar.api.util.LoggerI;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.PathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.List;
import java.util.Optional;

/**
 * Users application interface
 *
 * @author  archon
 * @version 1.0
 * @since   1.0
 */
@RestController
@RequestMapping("api/user")
public class UserController implements LoggerI {
    private final Logger logger = getLogger();
    private final UserRepository repository;
    private final Path imageStorageDir;

    public UserController(UserRepository repository,
                          @Value("${avatar-storage-dir}") Path imageStorageDir) {
        this.repository = repository;
        this.imageStorageDir = imageStorageDir;
    }

    /**
     * Creating folder for saving user avatar images
     */
    @PostConstruct
    public void ensureDirectoryExists() {
        final String path = imageStorageDir.toUri().getPath();
        if (!Files.exists(imageStorageDir)) {
            try {
                Files.createDirectories(this.imageStorageDir);
                logger.warn("Created avatar image storage directory: {}", path);
            } catch (IOException e) {
                logger.error("Error creating avatar image storage: {}", e.getMessage());
            }
        } else {
            logger.info("Detected avatar image storage directory: {}", path);
        }
    }

    /**
     * Full list of all users
     *
     * @return full users table data
     */
    @GetMapping("all")
    List<User> userList() {
        return repository.findAll();
    }

    /**
     * Save or update general user information
     * Null id tells to save a new one otherwise an existing id updates the record
     *
     * @param user to save
     * @return user id
     */
    @PostMapping("save")
    Integer save(@RequestBody User user) {
        return repository.save(user).getId();
    }

    /**
     * This enables you to perform POST requests against the "/image/YourID" path
     * It returns the name this image can be referenced on later
     *
     * @param image multipart file
     * @param id    user nickname
     * @return image name to refer to it
     * @throws IOException when fails to write file
     */
    @PostMapping(value = "/avatar/{nickname}", produces = MediaType.TEXT_PLAIN_VALUE)
    String uploadImage(@RequestBody MultipartFile image, @PathVariable("nickname") String id)
            throws IOException {
        final String fileExtension = Optional.ofNullable(image.getOriginalFilename())
                .flatMap(AvatarImageUtil::getFileExtension)
                .orElse("");

        final String targetFileName = id + "." + fileExtension;
        final Path targetPath = this.imageStorageDir.resolve(targetFileName);

        try (InputStream in = image.getInputStream()) {
            try (OutputStream out = Files.newOutputStream(targetPath, StandardOpenOption.CREATE)) {
                in.transferTo(out);
            }
        }

        return targetFileName;
    }

    /**
     * This enables you to download previously uploaded images
     *
     * @param fileName value from avatar column of the users table
     * @return image
     */
    @GetMapping("/image/{fileName}")
    ResponseEntity<Resource> downloadImage(@PathVariable("fileName") String fileName) {
        final Path targetPath = this.imageStorageDir.resolve(fileName);
        if (!Files.exists(targetPath)) {
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok(new PathResource(targetPath));
    }

    /**
     * Simply gets users id for api usage purposes
     *
     * @param nickname string desktop application-friendly user name
     * @return integer user id
     */
    @GetMapping("login/{nickname}")
    Integer login(@PathVariable String nickname) {
        return repository.findByName(nickname).getId();
    }
}
