package org.sandbox.calendar.api.domain.service;

import java.util.List;

/**
 * Csv file generator manager
 *
 * @author  archon
 * @version 1.0
 * @since   1.0
 */
public interface CsvServiceI {
    /**
     * Generalised method to write down to csv file any data structure
     *
     * @param data             content array
     * @param elementClassType class type to work with
     * @param <T>              for actual implementation this class should include annotated getters to write them down
     * @return operation status
     */
    <T> boolean go(List<T> data, Class<T> elementClassType);
}
