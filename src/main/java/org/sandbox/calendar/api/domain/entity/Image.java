package org.sandbox.calendar.api.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;

import java.util.Objects;

/**
 * Images in folder
 *
 * @author  archon
 * @version 1.0
 * @since   1.0
 */
@Entity
@Table(name = "image_gallery")
public class Image {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name = "image_path")
    private String name;
    @Column(name = "res_x")
    private Integer resizedResolutionX;
    @Column(name = "res_y")
    private Integer resizedResolutionY;
    @Column(name = "gallery_column")
    private Integer coordinateX;
    @Column(name = "gallery_row")
    private Integer coordinateY;
    @ManyToOne
    @JoinColumn(name = "folder_fk")
    @JsonIgnore
    private Folder folder;

    public Image() {
    }

    public Image(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Image(Integer id,
                 String name,
                 Integer resizedResolutionX,
                 Integer resizedResolutionY,
                 Integer coordinateX,
                 Integer coordinateY,
                 Folder folder) {
        this.id = id;
        this.name = name;
        this.resizedResolutionX = resizedResolutionX;
        this.resizedResolutionY = resizedResolutionY;
        this.coordinateX = coordinateX;
        this.coordinateY = coordinateY;
        this.folder = folder;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Folder getFolder() {
        return folder;
    }

    public void setFolder(Folder folder) {
        this.folder = folder;
    }

    public Integer getResizedResolutionX() {
        return resizedResolutionX;
    }

    public void setResizedResolutionX(Integer resizedResolutionX) {
        this.resizedResolutionX = resizedResolutionX;
    }

    public Integer getResizedResolutionY() {
        return resizedResolutionY;
    }

    public void setResizedResolutionY(Integer resizedResolutionY) {
        this.resizedResolutionY = resizedResolutionY;
    }

    public Integer getCoordinateX() {
        return coordinateX;
    }

    public void setCoordinateX(Integer coordinateX) {
        this.coordinateX = coordinateX;
    }

    public Integer getCoordinateY() {
        return coordinateY;
    }

    public void setCoordinateY(Integer coordinateY) {
        this.coordinateY = coordinateY;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Image image = (Image) o;
        return Objects.equals(id, image.id) && Objects.equals(name, image.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }

    @Override
    public String toString() {
        return "Image{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
