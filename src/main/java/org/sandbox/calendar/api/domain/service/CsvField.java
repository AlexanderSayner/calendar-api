package org.sandbox.calendar.api.domain.service;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Marks getters which ones will be exported in csv file
 * But it accepts any method so
 * there is might be some annotation processor to prevent any non getter methods
 *
 * @author  archon
 * @version 1.0
 * @since   1.0
 */
@Target(METHOD)
@Retention(RUNTIME)
public @interface CsvField {
    int sort() default 0;
}
