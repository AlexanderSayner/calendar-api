package org.sandbox.calendar.api.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;

import java.util.List;
import java.util.Objects;

/**
 * Users folder for some notes and sketches
 *
 * @author  archon
 * @version 1.0
 * @since   1.0
 */
@Entity
@Table(name = "folders")
public class Folder {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name = "folder_name")
    private String name;
    @OneToMany(mappedBy = "folder")
    @JsonIgnore
    private List<Image> images;
    @OneToOne(mappedBy = "folder")
    @JsonIgnore
    private FolderTable table;
    @ManyToOne
    @JoinColumn(name = "user_fk")
    @JsonIgnore
    private User user;

    public Folder() {
    }

    public Folder(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Image> getImages() {
        return images;
    }

    public void setImages(List<Image> images) {
        this.images = images;
    }

    public FolderTable getTable() {
        return table;
    }

    public void setTable(FolderTable table) {
        this.table = table;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Folder folder = (Folder) o;
        return Objects.equals(id, folder.id) && Objects.equals(name, folder.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }

    @Override
    public String toString() {
        return "Folder{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
