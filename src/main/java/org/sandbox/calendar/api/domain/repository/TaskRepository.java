package org.sandbox.calendar.api.domain.repository;

import org.sandbox.calendar.api.domain.entity.Task;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Data access object for a task entity
 *
 * @author  archon
 * @version 1.0
 * @since   1.0
 */
@Repository
public interface TaskRepository extends JpaRepository<Task, Integer> {
    List<Task> findAllByUserId(Integer userId);
}
