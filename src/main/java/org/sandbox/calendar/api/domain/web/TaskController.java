package org.sandbox.calendar.api.domain.web;

import jakarta.websocket.server.PathParam;
import org.sandbox.calendar.api.domain.entity.Task;
import org.sandbox.calendar.api.domain.repository.TaskRepository;
import org.sandbox.calendar.api.domain.service.CsvServiceI;
import org.sandbox.calendar.api.domain.service.UserServiceI;
import org.sandbox.calendar.api.domain.service.XlsServiceI;
import org.sandbox.calendar.api.util.LoggerI;
import org.slf4j.Logger;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Tasks application interface
 *
 * @author  archon
 * @version 1.0
 * @since   1.0
 */
@RestController
@RequestMapping("api/task")
public class TaskController implements LoggerI {
    private final Logger logger = getLogger();
    private final TaskRepository repository;
    private final UserServiceI userService;
    private final CsvServiceI csvService;
    private final XlsServiceI xlsService;

    public TaskController(TaskRepository repository,
                          UserServiceI userService,
                          CsvServiceI csvService,
                          XlsServiceI xlsService) {
        this.repository = repository;
        this.userService = userService;
        this.csvService = csvService;
        this.xlsService = xlsService;
    }

    /**
     * List of all tasks
     *
     * @return full database data for tasks table
     */
    @GetMapping("all")
    List<Task> taskList() {
        return repository.findAll();
    }

    /**
     * Adding new or updating existing by passing null id or saved id accordingly
     *
     * @param userId saved user id
     * @param task   to save or update
     * @return saved task id
     */
    @PostMapping("save")
    Integer save(@PathParam("userId") Integer userId, @RequestBody Task task) {
        if (userService.setUser(userId, task::setUser) != 0) {
            logger.info("Saving task without user");
        }
        return repository.save(task).getId();
    }

    /**
     * Saved tasks for exising user
     * General tasks are saved without user id
     *
     * @param userId      saved user id
     * @param showGeneral optional, false by default
     * @return tasks array
     */
    @GetMapping("list/{userId}")
    List<Task> userTasks(@PathVariable Integer userId, @PathParam("showGeneral") boolean showGeneral) {
        final List<Task> tasks = repository.findAllByUserId(userId);
        if (showGeneral) {
            tasks.addAll(repository.findAllByUserId(null));
        }
        return tasks;
    }

    /**
     * All tasks to csv
     *
     * @return status message
     */
    @GetMapping("csv")
    public String csv() {
        final boolean go = csvService.go(repository.findAll(), Task.class);
        if (!go) {
            return "failure";
        }
        return "Csv file generated";
    }

    /**
     * All tasks to xls
     *
     * @return status message
     */
    @GetMapping("xls")
    public String xls() {
        final boolean go = xlsService.go(repository.findAll(), Task.class);
        if (!go) {
            return "failure";
        }
        return "Xls file generated";
    }
}
