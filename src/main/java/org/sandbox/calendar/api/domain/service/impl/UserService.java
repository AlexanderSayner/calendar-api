package org.sandbox.calendar.api.domain.service.impl;

import org.sandbox.calendar.api.domain.entity.User;
import org.sandbox.calendar.api.domain.entity.helper.UserRelatedEntityI;
import org.sandbox.calendar.api.domain.repository.UserRepository;
import org.sandbox.calendar.api.domain.service.UserServiceI;
import org.sandbox.calendar.api.util.LoggerI;
import org.slf4j.Logger;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * User service implementation
 *
 * @author  archon
 * @version 1.0
 * @since   1.0
 */
@Service
public class UserService implements UserServiceI, LoggerI {
    private final Logger logger = getLogger();
    private final UserRepository repository;

    public UserService(UserRepository repository) {
        this.repository = repository;
    }

    /**
     * Passing throw user id which one equals zero or null
     * means to skip trying to find user entity in database.
     * Do not relay on this logic as interface.
     *
     * @param userId actual user id
     * @param entity implemented logic for setting up a user entity
     * @return 0 if user is set or userId is null or 0, -1 if there is no user with such id
     */
    @Override
    public int setUser(Integer userId, UserRelatedEntityI entity) {
        if (userId != null && !userId.equals(0)) {
            final Optional<User> user = repository.findById(userId);
            if (user.isEmpty()) {
                logger.warn("No user with id={} found", userId);
                return -1;
            }
            entity.setUser(user.get());
        }
        return 0;
    }
}
