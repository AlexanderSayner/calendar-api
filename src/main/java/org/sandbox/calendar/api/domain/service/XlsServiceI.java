package org.sandbox.calendar.api.domain.service;

/**
 * Interface marker for xls service bean
 *
 * @author  archon
 * @version 1.0
 * @since   1.0
 */
public interface XlsServiceI extends CsvServiceI {
}
