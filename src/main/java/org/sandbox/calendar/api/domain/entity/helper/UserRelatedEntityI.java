package org.sandbox.calendar.api.domain.entity.helper;

import org.sandbox.calendar.api.domain.entity.User;

/**
 * Initialising user relation
 *
 * @author  archon
 * @version 1.0
 * @since   1.0
 */
@FunctionalInterface
public interface UserRelatedEntityI {
    void setUser(User user);
}
