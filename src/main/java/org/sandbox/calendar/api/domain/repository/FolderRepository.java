package org.sandbox.calendar.api.domain.repository;

import org.sandbox.calendar.api.domain.entity.Folder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Data access object for a folder entity
 *
 * @author  archon
 * @version 1.0
 * @since   1.0
 */
@Repository
public interface FolderRepository extends JpaRepository<Folder, Integer> {
}
