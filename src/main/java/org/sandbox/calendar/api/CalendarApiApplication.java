package org.sandbox.calendar.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Main app starter class
 *
 * @author  archon
 * @version 1.0
 * @since   1.0
 */
@SpringBootApplication
public class CalendarApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(CalendarApiApplication.class, args);
    }

}
