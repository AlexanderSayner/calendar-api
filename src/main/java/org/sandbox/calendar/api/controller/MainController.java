package org.sandbox.calendar.api.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * Template free fully static view web controller
 *
 * @author  archon
 * @version 1.0
 * @since   1.0
 */
@Controller
public class MainController {

    @GetMapping("favicon.ico")
    String getFaviconPngImage() {
        return "forward:/static/favicon.png";
    }

    @GetMapping
    String mainPage() {
        return "forward:/static/index.html";
    }
}
