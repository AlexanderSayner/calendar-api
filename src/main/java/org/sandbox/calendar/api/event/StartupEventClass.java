package org.sandbox.calendar.api.event;

import org.sandbox.calendar.api.util.LoggerI;
import org.slf4j.Logger;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.boot.system.SystemProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.context.event.EventListener;
import org.springframework.core.SpringVersion;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * Print welcome message in application log
 *
 * @author  archon
 * @version 1.0
 * @since   1.0
 */
@Component
public class StartupEventClass implements LoggerI {
    private final Logger log = getLogger();

    private final ApplicationContext appContext;

    public StartupEventClass(ApplicationContext appContext) {
        this.appContext = appContext;
    }

    @EventListener(ApplicationReadyEvent.class)
    public void doSomethingAfterStartup() {
        final Environment environment = appContext.getEnvironment();

        log.info("""

                        ==============================================================================================================
                        \tApplication '{}' just started up at {}://{}:{}
                        \tSpring version: '{}'
                        \tJava version: '{}'
                        ==============================================================================================================
                        """,
                environment.getProperty("spring.application.name"),
                getProtocol(environment),
                getIp(),
                getPort(environment),
                SpringVersion.getVersion(),
                SystemProperties.get("java.version")
        );
    }

    /**
     * get protocol
     *
     * @param environment created in main()
     * @return not null string
     */
    private String getProtocol(final Environment environment) {
        String protocol = "http";
        if (environment.getProperty("server.ssl.key-store") != null) {
            protocol = "https";
        }
        return protocol;
    }

    /**
     * get ip
     *
     * @return not null string
     */
    private String getIp() {
        String hostAddress = "127.0.0.1";
        try {
            hostAddress = InetAddress.getLocalHost().getHostAddress();
        } catch (UnknownHostException e) {
            log.warn("Can not get ip: {}", e.getMessage());
        }
        return hostAddress;
    }

    /**
     * get port
     *
     * @return string
     */
    private String getPort(final Environment environment) {
        return environment.getProperty("server.port");
    }
}
