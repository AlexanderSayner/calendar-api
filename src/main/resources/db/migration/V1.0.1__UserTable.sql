CREATE TABLE IF NOT EXISTS users
(
    id     SERIAL PRIMARY KEY,
    name   VARCHAR(255) NOT NULL,
    avatar VARCHAR(255),
    date   timestamptz  NOT NULL
);
