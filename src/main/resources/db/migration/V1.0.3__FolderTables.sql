CREATE TABLE IF NOT EXISTS folders
(
    id          SERIAL PRIMARY KEY,
    folder_name VARCHAR(255) NOT NULL,
    user_fk     INTEGER REFERENCES users (id)
);
CREATE TABLE IF NOT EXISTS image_gallery
(
    id             SERIAL PRIMARY KEY,
    image_path     VARCHAR(255) NOT NULL,
    res_x          INTEGER,
    res_y          INTEGER,
    gallery_column INTEGER,
    gallery_row    INTEGER,
    folder_fk      INTEGER REFERENCES folders (id)
);
CREATE TABLE IF NOT EXISTS folder_tables
(
    id            SERIAL PRIMARY KEY,
    table_columns INTEGER,
    table_rows    INTEGER,
    folder_fk     INTEGER REFERENCES folders (id)
);
CREATE TABLE IF NOT EXISTS folder_table_cells
(
    id           SERIAL PRIMARY KEY,
    cell_column  INTEGER,
    cell_row     INTEGER,
    text_content VARCHAR(255),
    table_fk     INTEGER REFERENCES folder_tables (id)
);
