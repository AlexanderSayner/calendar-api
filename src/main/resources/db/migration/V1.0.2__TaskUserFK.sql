ALTER TABLE tasks
    ADD COLUMN IF NOT EXISTS user_fk INTEGER REFERENCES users (id);
ALTER TABLE tasks
    ADD CONSTRAINT fk_tasks_users FOREIGN KEY (user_fk) REFERENCES users (id);
