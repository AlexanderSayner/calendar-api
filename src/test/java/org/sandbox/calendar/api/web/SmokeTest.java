package org.sandbox.calendar.api.web;

import org.junit.jupiter.api.Test;
import org.sandbox.calendar.api.H2JpaConfig;
import org.sandbox.calendar.api.controller.MainController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Web controller smoke test
 *
 * @author  archon
 * @version 1.0
 * @since   1.0
 */
@SpringBootTest(classes = {H2JpaConfig.class})
@ActiveProfiles("test")
public class SmokeTest {
    @Autowired
    private MainController mainController;

    @Test
    public void mainControllerLoads() {
        assertThat(mainController).isNotNull();
    }
}
