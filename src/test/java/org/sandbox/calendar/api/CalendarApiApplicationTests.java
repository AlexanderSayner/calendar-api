package org.sandbox.calendar.api;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

/**
 * Application health test
 *
 * @author  archon
 * @version 1.0
 * @since   1.0
 */
@SpringBootTest(classes = {H2JpaConfig.class})
@ActiveProfiles("test")
class CalendarApiApplicationTests {

    @Test
    void contextLoads() {
    }

}
