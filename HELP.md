# Getting Started

### API Requests

```http request
POST http://localhost:9080/api/user/save
Accept: application/json
Content-Type: application/json

{
  "id": null,
  "name": "Alex Säyner",
  "date": "1999-11-28T17:04:23.712Z"
}
```

```http request
GET http://localhost:9080/api/user/all
Accept: application/json
```

```http request
GET http://localhost:9080/api/user/login/Alex Säyner
Accept: application/json
```

```shell
curl -X POST -F "image=@/home/archon/Downloads/1.jpg" http://cms.desktop:9080/api/user/avatar/origin
```

```http request
GET http://localhost:9080/api/user/image/origin.jpg
Accept: image/jpeg
```

```http request
POST http://localhost:9080/api/task/save?userId=1
Accept: application/json
Content-Type: application/json

{
  "id": null,
  "name": "task with fk",
  "description": "testing foreign key in the task table",
  "date": "2024-10-11T18:11:16.237Z"
}
```

```http request
GET http://localhost:9080/api/task/all
Accept: application/json
```

```http request
GET http://localhost:9080/api/task/list/1?showGeneral=true
Accept: application/json
```

### Reference Documentation

For further reference, please consider the following sections:

* [Official Gradle documentation](https://docs.gradle.org)
* [Spring Boot Gradle Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/3.1.4/gradle-plugin/reference/html/)
* [Create an OCI image](https://docs.spring.io/spring-boot/docs/3.1.4/gradle-plugin/reference/html/#build-image)
* [Spring Data JPA](https://docs.spring.io/spring-boot/docs/3.1.4/reference/htmlsingle/index.html#data.sql.jpa-and-spring-data)
* [Spring Web](https://docs.spring.io/spring-boot/docs/3.1.4/reference/htmlsingle/index.html#web)
* [Flyway Migration](https://docs.spring.io/spring-boot/docs/3.1.4/reference/htmlsingle/index.html#howto.data-initialization.migration-tool.flyway)

### Guides

The following guides illustrate how to use some features concretely:

* [Accessing Data with JPA](https://spring.io/guides/gs/accessing-data-jpa/)
* [Building a RESTful Web Service](https://spring.io/guides/gs/rest-service/)
* [Serving Web Content with Spring MVC](https://spring.io/guides/gs/serving-web-content/)
* [Building REST services with Spring](https://spring.io/guides/tutorials/rest/)

### Additional Links

These additional references should also help you:

* [Gradle Build Scans – insights for your project's build](https://scans.gradle.com#gradle)

